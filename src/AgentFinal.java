import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AgentFinal extends Application {

	GraphicsContext gc;
	int x = 50;
	int y = 50;
	Agent myAgent;
	int[][] gridData = { { 0, 1, 1, 1, 1, 0, 1, 0, 0, 1 }, { 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 },
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 }, { 1, 1, 0, 0, 0, 1, 0, 1, 1, 0 }, { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
			{ 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 }, { 1, 1, 0, 1, 0, 1, 0, 0, 0, 0 }, { 0, 0, 0, 1, 0, 1, 0, 1, 0, 0 },
			{ 1, 0, 1, 1, 0, 1, 0, 0, 0, 1 }, { 0, 0, 1, 1, 1, 1, 0, 0, 1, 1 } };

	double[][] Q = new double[100][4];
	Random rand = new Random();
	double alfa = 0.1;
	double gamma = 0.9;
	int goalState = 0;
	int startState = 76;
	int[] holeState = { 66, 55 , 44};
	int episodes = 200;
	int moves = 0;
	Circle agentC;
	Timeline timeline;
	AnimationTimer timer;

	Label labelXY;
	Label labelState;
	Label labelGrid;
	Canvas canvas;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		BorderPane rootNode = new BorderPane();
		canvas = new Canvas(500, 500);
		labelXY = new Label();
		labelState = new Label();
		labelGrid = new Label();
		Scene scene = new Scene(rootNode, 700, 500);

		VBox box = new VBox();
		box.getChildren().addAll(labelXY, labelState, labelGrid);
		rootNode.setCenter(box);
		rootNode.setLeft(canvas);
		stage.setTitle("Agents");
		stage.sizeToScene();
		stage.setScene(scene);

		agentC = new Circle(10);
		agentC.setFill(Color.ORANGE);
		rootNode.getChildren().add(agentC);

		stage.show();

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if (event.getCode().equals(KeyCode.UP)) {
					MoveAgent(myAgent, 0);
					Animation();
				}
				if (event.getCode().equals(KeyCode.DOWN)) {
					MoveAgent(myAgent, 1);
					Animation();
				}
				if (event.getCode().equals(KeyCode.LEFT)) {
					MoveAgent(myAgent, 2);
					Animation();
				}
				if (event.getCode().equals(KeyCode.RIGHT)) {
					MoveAgent(myAgent, 3);
					Animation();
				}
				if (event.getCode().equals(KeyCode.Q)) {
					printTable(Q);
				}
				if (event.getCode().equals(KeyCode.D)) {
					AgentTickNonRandom();
				}
				if (event.getCode().equals(KeyCode.L)) {
					behavior();
				}
				if (event.getCode().equals(KeyCode.G)) {
					AgentOneRandomEpisode();
				}
				if (event.getCode().equals(KeyCode.P)) {
					AgentOneRandomMove();
					Animation();
				}
				if (event.getCode().equals(KeyCode.W)) {
					moves = 0;
					myAgent = StateToCoordinates(startState);
					reDrawNewGrid();
				}
			}
		});

		gc = canvas.getGraphicsContext2D();
		myAgent = StateToCoordinates(startState);
		agentC.setTranslateX((myAgent.x * 50) + 25);
		agentC.setTranslateY((myAgent.y * 50) + 25);

		DrawGrid(gc);
		Init_Q();
	}

	public void printGrid() {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				System.out.println("grid[" + i + "][" + j + "] = " + gridData[i][j]);
			}
		}
	}

	public void Animation() {
		timeline = new Timeline();
		timeline.setCycleCount(1);
		timeline.setAutoReverse(false);

		EventHandler<ActionEvent> onFinished = new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				timeline.stop();
			}
		};

		KeyValue keyValueX = new KeyValue(agentC.translateXProperty(), myAgent.x * 50 + 25);
		KeyValue keyValueY = new KeyValue(agentC.translateYProperty(), myAgent.y * 50 + 25);

		Duration duration1 = Duration.millis(125);
		Duration duration2 = Duration.millis(125);

		KeyFrame keyFrame1 = new KeyFrame(duration1, onFinished, keyValueX);
		KeyFrame keyFrame2 = new KeyFrame(duration2, onFinished, keyValueY);

		timeline.getKeyFrames().addAll(keyFrame1, keyFrame2);

		timeline.playFromStart();
	}
	
	public boolean checkForHoleState(int nextState){
		for(int i = 0; i < holeState.length; i++){
			if(nextState == holeState[i]){
				return true;
			}
		}
		return false;
	}


	public void behavior() {
		int state = CoordinatesToState(myAgent);
		for (int i = 0; i < episodes; i++) {
			do {
				state = CoordinatesToState(myAgent);
				int action = getRandomNumberInRange(0, 3);
				int sLast = state;
				int aLast = action;
				MoveAgent(myAgent, action);
				int nextState = CoordinatesToState(myAgent);

				double R = 0;
				if (nextState == goalState) {
					R = 1;
				} else if (checkForHoleState(nextState)) {
					R = -1;
				} else if (nextState == state) {
					R = -1;
				}
				Q[sLast][aLast] = (1 - alfa) * Q[sLast][aLast] + alfa * (R + gamma * maxQ(nextState));
				state = nextState;

			} while (state != goalState);
			System.out.println((i + 1) + " episode done");
			System.out.println("Moves: " + moves);
			System.out.println("Success state: " + CoordinatesToState(myAgent));
			moves = 0;
			myAgent = StateToCoordinates(startState);
			System.out.println("state: " + state);
		}
	}

	public void AgentOneRandomEpisode() {
		int state = CoordinatesToState(myAgent);
		do {
			state = CoordinatesToState(myAgent);
			int action = getRandomNumberInRange(0, 3);
			int sLast = state;
			int aLast = action;
			MoveAgent(myAgent, action);
			int nextState = CoordinatesToState(myAgent);

			double R = 0;
			if (nextState == goalState) {
				R = 1;
			} else if (checkForHoleState(nextState)) {
				R = -1;
			} else if (nextState == state) {
				R = -1;
			}
			Q[sLast][aLast] = (1 - alfa) * Q[sLast][aLast] + alfa * (R + gamma * maxQ(nextState));
			state = nextState;

		} while (state != goalState);
		System.out.println(" episode done");
		System.out.println("Moves: " + moves);
		System.out.println("Success state: " + CoordinatesToState(myAgent));
		moves = 0;
		myAgent = StateToCoordinates(startState);
		System.out.println("state: " + state);
	}

	public void AgentOneRandomMove() {
		int state = CoordinatesToState(myAgent);
		state = CoordinatesToState(myAgent);
		int action = getRandomNumberInRange(0, 3);
		int sLast = state;
		int aLast = action;
		MoveAgent(myAgent, action);
		int nextState = CoordinatesToState(myAgent);
		double R = 0;
		if (nextState == goalState) {
			R = 1;
		} else if (checkForHoleState(nextState)) {
			R = -1;
		} else if (nextState == state) {
			R = -1;
		}
		Q[sLast][aLast] = (1 - alfa) * Q[sLast][aLast] + alfa * (R + gamma * maxQ(nextState));
		state = nextState;
		System.out.println("Moves: " + moves);
		System.out.println("Current state: " + state);
	}

	public void AgentTickNonRandom() {
		int currentState = CoordinatesToState(myAgent);
		int action = selectNonRandomAction(currentState);
		MoveAgent(myAgent, action);
		Animation();
	}

	public double maxQ(int state) {
		double maxQ = Q[state][0];
		for (int i = 0; i < 4; i++) {
			if (maxQ < Q[state][i]) {
				maxQ = Q[state][i];
			}
		}
		return maxQ;
	}

	public int selectNonRandomAction(int state) {
		double maxQ = 0;
		int maxA = 0;
		for (int a = 0; a < 4; a++) {
			if (Q[state][a] > maxQ) {
				maxQ = Q[state][a];
				maxA = a;
			}
		}
		// System.out.println("maxQ: " + maxQ);
		// System.out.println("Action : " + maxA);
		return maxA;
	}

	private static int getRandomNumberInRange(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	private void Init_Q() {
		for (int i = 0; i < 10 * 10; i++) {
			for (int j = 0; j < 4; j++) {
				Q[i][j] = 1.1;
			}
		}
		Agent dummyAgent = StateToCoordinates(goalState);
		Agent newDummyAgent;

		if (goalState + 1 < 100) {
			newDummyAgent = StateToCoordinates(goalState + 10);
			if (newDummyAgent.y < 10 && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				Q[goalState + 10][0] = 0;
			}
		}

		if (goalState - 1 > 0) {
			newDummyAgent = StateToCoordinates(goalState - 10);
			if (newDummyAgent.y > 0 && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				Q[goalState - 10][1] = 0;
			}
		}

		if (goalState + 1 < 100) {
			newDummyAgent = StateToCoordinates(goalState + 1);
			if (dummyAgent.y == newDummyAgent.y && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				Q[goalState + 1][2] = 0;
			}
		}

		if (goalState - 1 > 0) {
			newDummyAgent = StateToCoordinates(goalState - 1);
			if (dummyAgent.y == newDummyAgent.y && gridData[newDummyAgent.y][newDummyAgent.x] == 0) {
				Q[goalState - 1][3] = 0;
			}
		}
		printTable(Q);
	}

	private void printTable(double[][] table) {
		System.out.println("     0    1    2    3");
		for (int i = 0; i < 10 * 10; i++) {
			if (i < 10) {
				System.out.print("" + i + "  |");
			} else {
				System.out.print("" + i + " |");
			}
			for (int j = 0; j < 4; j++) {
				System.out.print("" + table[i][j] + "  ");
			}
			System.out.println("");
		}
	}

	public int CoordinatesToState(Agent ag) {
		int state = 0;
		state = ag.y * 10 + ag.x;
		return state;
	}

	public Agent StateToCoordinates(int state) {
		Agent ag = new Agent(0, 0);
		ag.x = state % 10;
		ag.y = state / 10;

		return ag;
	}

	public void MoveAgent(Agent myAgent, int a) {
		moves++;
		switch (a) {
		// 0 = UP, 1 = Down, 2 = Left, 3 = Right;
		case 0:
			if (myAgent.y - 1 >= 0 && gridData[myAgent.y - 1][myAgent.x] != 1)
				myAgent.y -= 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: UP");
			break;
		case 1:
			if (myAgent.y + 1 <= 9 && gridData[myAgent.y + 1][myAgent.x] != 1)
				myAgent.y += 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: Down");
			break;
		case 2:
			if (myAgent.x - 1 >= 0 && gridData[myAgent.y][myAgent.x - 1] != 1)
				myAgent.x -= 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: Left");
			break;
		case 3:
			if (myAgent.x + 1 <= 9 && gridData[myAgent.y][myAgent.x + 1] != 1)
				myAgent.x += 1;
			textToLabel(myAgent);
			// System.out.println("Action taken: Right");
			break;
		}
	}

	public void textToLabel(Agent ag) {
		labelXY.setText("X: " + ag.x + " Y: " + ag.y);
		labelState.setText("State: " + CoordinatesToState(ag));
		labelGrid.setText("Grid:" + gridData[ag.y][ag.x]);
	}

	public void reDrawNewGrid() {
		gc.clearRect(0, 0, 500, 500);
		DrawGrid(gc);
		agentC.setTranslateX((myAgent.x * 50) + 25);
		agentC.setTranslateY((myAgent.y * 50) + 25);
		gc.fillOval(myAgent.x * 50 + 10, myAgent.y * 50 + 10, 30, 30);
	}

	public void DrawGrid(GraphicsContext g) {
		int i = 0;
		int j = 0;
		g.setLineWidth(2);
		g.setStroke(Color.BLACK);
		for (i = 0; i < 11; i++) {
			g.strokeLine(0, i * y, 500, i * y);
			for (j = 0; j < 11; j++) {
				if (i < 10 && j < 10 && gridData[i][j] == 1) {
					g.setFill(Color.BLUE);
					g.fillRect(j * x + 1, i * y + 1, 48, 48);
				}
				g.strokeLine(j * x, 0, j * x, 500);
			}
		}
		Agent dummy;

		dummy = StateToCoordinates(goalState);
		g.setFill(Color.RED);
		g.fillRect(dummy.x * 50 + 1, dummy.y * 50 + 1, 48, 48);
		
		g.setFill(Color.BLACK);
		for(int d = 0; d < holeState.length; d++){
			dummy = StateToCoordinates(holeState[d]);
			g.fillRect(dummy.x * 50 + 1, dummy.y * 50 + 1, 48, 48);
		}

		dummy = StateToCoordinates(startState);
		g.setFill(Color.GREEN);
		g.fillRect(dummy.x * 50 + 1, dummy.y * 50 + 1, 48, 48);
	}
}
